/*===========================================================================*\

Author: Matthias W. Smith
Email : mwmsith2@uw.edu
Date  : 14/2/14

Notes :
	This simulates FID using the Bloch equations and numerical integration.
	There is a default parameter set which can be overridden by setting up a 
	config file and passing it as the command line argument.  If no argument is
	given, it loads a default config file called "sim_config.json".  If no config
	file is desired, then pass "none" to use the default params.

Dependencies:
	C++11 support
	Boost Libraries
	Armadillo Linalg Library

Compiling:
	I use clang++ on my mac, g++ also works though the -std=c++11 may need to be
	changed to -std=c++0x

	clang++ -std=c++11 -O4 generate_fids.cxx -o generate_fids

Example Usages:
	
	./generate_fids none
	./generate_fids
	./generate_fids my_params.json

/*===========================================================================*/



/*--- STD Includes ----------------------------------------------------------*/

#include <fstream>
#include <string>
#include <vector>
#include <chrono>
#include <thread>
#include <algorithm>
#include <random>
#include <cmath>


/*--- Other Includes --------------------------------------------------------*/

#include <boost/numeric/odeint.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <armadillo>


/*--- Namespaces ------------------------------------------------------------*/

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;
using namespace std::chrono;
using namespace boost::numeric::odeint;
using namespace boost::property_tree;


typedef vector<double> Vec;


/*--- Global Namespace ------------------------------------------------------*/

namespace{

	// Initilized to reasonable defaults
	int nfids = 10000;
	int nPoints = 100000;
	int reduction = 10;
	double ti = -1.0;
	double tTotal = 10.0;
	double tf = ti + tTotal;
	double dt = 1e-5;

	double fL = 997.;
	double fRef = 950.;
	double s2n  = 100.;

	int nSteps = 5;
	double Brange = 100e-6;

	//strength and duration pulse; OmegaR*tpulse should equal 1/4 for pi-pulse
	double OmegaR = 50.0;
	double tPulse = 0.005;
	double gammag = 1.0; //42.6
	double gamma1 = 0.05;
	double gamma2 = 0.05;
	double pi2 = 6.283185307179586;


	double x;
}

/*---Inline Fucntions--------------------------------------------------------*/

// A template function to handle vector addition.
template <typename T>
inline vector<T>& operator+(vector<T>& a, vector<T>& b)
{
    assert(a.size() == b.size());

    transform(a.begin(), a.end(), b.begin(), a.begin(), std::plus<T>());
    return a;
}

// A template function to handle vector subtraction.
template <typename T>
inline vector<T>& operator-(vector<T>& a, vector<T>& b)
{
    assert(a.size() == b.size());

    transform(a.begin(), a.end(), b.begin(), a.begin(), std::minus<T>());
    return a;
}

// A template function to handle vector multiplying with a scalar.
template <typename T>
inline vector<T>& operator*(T c, vector<T>& a)
{
	for (auto it = a.begin(); it != a.end(); ++it){
		*it = c * (*it);
	}
	return a;
}

// Define the cross product for 3-vectors.
inline void cross(const Vec& u, const Vec& v, Vec& res){
	res[0] = u[1] * v[2] - u[2] * v[1];
	res[1] = u[2] * v[0] - u[0] * v[2];
	res[2] = u[0] * v[1] - u[1] * v[0];
} 

// Function which returns time dependent Bfield.
inline Vec Bfield(const double& x, const double& t){

	// Made static to save on memory calls.
	static Vec a = {0., 0., 0.}; // holds constant external field
	static Vec b = {0., 0., 0.}; // for time dependent B field
	static double w = pi2 * fRef;

	// Return static external field if after the pulsed field.
	if (t >= tPulse){
		return a;

	// Set the fields if the simulation is just starting.
	}else if (t <= ti + dt){
		a[2] = pi2 * (1. + x) * fL;
		b[2] = pi2 * (1. + x) * fL;
	}

	// Return static field if the time is before the pulsed field.
	if (t < 0.0) return a;

	// If none of the above, return the time-dependent, pulsed field.
	b[0] = OmegaR * cos(w * t);
	b[1] = OmegaR * sin(w * t);
	return b;
}

// The time evolution equation for the fields.
void bloch(Vec const &s, Vec &dsdt, double t){

	// Again static to save time on memory allocations.
	static Vec b = {{0., 0., 0.}};   // Bfield
	static Vec s1 = {{0., 0., 0.}};  // Cross product piece of spin
	static Vec s2 = {{0., 0., 0.}};  // Relaxation piece of spin
	static double a1 = pi2 * gamma1; // Relaxation time #1
	static double a2 = pi2 * gamma2; // Relaxation time #2

	// Only need to update the Bfield before the pulsed kick has finished.
	if (t <= tPulse + dt) b = Bfield(::x, t);
	
	// Set the relaxtion bits of the field.
	s2[0] = a2 * s[0];
	s2[1] = a2 * s[1];
	s2[2] = a1 * s[2] - a1;

	// Calculate the cross product.
	cross(b, s, s1);

	// Set the differential to be integrated.
   	dsdt = s1 - s2;
}


/*--- Function declarations -------------------------------------------------*/

vector<double> lowPassFilter(vector<double>& s);
void addNoise(vector<double>& s);
void printer(Vec const &s, double t);
void loadParams(string filename);


/*---------------------------------------------------------------------------*/
/*---Main Body---------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

int main(int argc, char** argv) {

	// Record the starting timestamp
	time_point <steady_clock> start, end;
	start = steady_clock::now();

	// Initilize the offset parameter.
	::x = 0.0;

	// Set the config filename
	string filename;
	if (argc < 2){
		filename = string("sim_config.json");
	} else {
		filename = string(argv[1]);
	}

	// Load the config parameters
	loadParams(filename);

	// FID loop
  for (int i = 0; i < nfids; i++){

  	if (nSteps > 1){
	  	// Set the field gradient term for this step
	 		::x = -0.5 * Brange;

	 		for (int j = 0; j < nSteps; j++){
	 			// Initialize magnetic polarization
		   	Vec s = {0., 0., 1.}; 

		   	// Constant time step integration
				integrate_const(runge_kutta4<Vec>(), bloch, s, ti, tf, dt, printer);
	 			::x += Brange / (nSteps - 1); // Update gradient term
			}

		} else { // If no gradients need to be processed.
			// Initial magnetic polarization
		  Vec s = {0., 0., 1.}; 

		  // Constant time step integration
			integrate_const(runge_kutta4<Vec>(), bloch, s, ti, tf, dt, printer);
		}
  }

 	// Get the final timestamp
 	end = steady_clock::now();

 	duration<double> elapsed_secs = end - start;
 	cout << "Simulated " << nfids << " FIDs in " << elapsed_secs.count();
 	cout << " seconds." << endl;

 	return 0;
}


/*--- Function Implementations ----------------------------------------------*/

// Low pass filter to suppress the higher frequency introducing in mixing down.
vector<double> lowPassFilter(vector<double>& s){

	// Store the filter statically though this might be a minimal speed boost.
	static vector<double> filter;
	static double wCut = pi2 * fRef;

	// Define the filter if not defined.  Using 3rd order Butterworth filter.
	if (filter.size() == 0){

		filter.resize(nPoints);
		int i = 0;
		int j = nPoints-1;
		double temp;

		// The filter is symmetric, so we can fill both sides in tandem.
		while (i < nPoints/2){
			temp = pow((pi2 * i) / (dt * nPoints * wCut), 6); // scaling term
			filter[i] = pow(1.0 / (1.0 + temp), 0.5);
			filter[j--] = filter[i++];
		}
	}

	// Copy the vectors to Armadillo formats first.
	arma::vec v1(&s[0], s.size(), false); 
	arma::vec v2(&filter[0], filter.size(), false);

	// Now get the FFT
	arma::cx_vec fft = arma::fft(v1);

	// Multiply the FFT and the filter element wise
	fft = fft % v2;

	// This confusing oneliner, get the inverse FFT and converts to a std::vector
	return arma::conv_to<vector<double>>::from(arma::real(arma::ifft(fft)));
}


// A function that simulates gaussian noise on top of our signal.
void addNoise(vector<double>& s){

	// Set up our static variables
	static double sigma = -1.0;
	static std::default_random_engine gen;

	// Set sigma if not set already
	if (sigma <= 0.0){
		// Set the scale from a simulate FID.
		double max = *std::max_element(s.begin(), s.end());
		// Normalize with the signal to noise.
		sigma = max / s2n;
	}

	// Initialize the noise distribution.
	static std::normal_distribution<double> gaus(0.0, sigma);

	// Noise loop
	for (auto it = s.begin(); it != s.end(); it++){
		*it += gaus(gen);
	}
	// all noisied up
}

// Printer function is called to do stuff each step of integration.
void printer(Vec const &s , double t){

	// Initialized static memory cache.
	static int count = 0;
	static int index = 0;
	static int fidNum = 0;
	static int stepNum = 0;
	static int refCount = (tTotal / dt) / nPoints;
	static vector<double> spin (nPoints, 0.0);
	static vector<double> time (nPoints, 0.0);
	static vector<double> spinSum (nPoints, 0.0);
	static vector<double> cosCache;
	static ofstream out;

	// Cache the cosine function for mixing.
	if (cosCache.size() == 0){
		cosCache.reserve(nPoints);
		double temp = t;
		for (int i = 0; i < nPoints; i++){
			cosCache.push_back(cos(pi2 * fRef * temp));
			temp += dt * refCount;
		}
	}

	// Time is equal to one of our time steps, do stuff.
	if (count++ % refCount == 0){ 

		// Record spin in the y-direction and mix down
		spin[index] = s[1] * cosCache[index]; 

		// Record the time
	  time[index++] = t;


	  // If the FID is done, do more stuff.
		if (t == tf){

			// Reset the spinSum if we are starting a new FID
			if (stepNum == 0) spinSum.assign(nPoints, 0.0);

			// If the FID has no more gradient steps left
			if (stepNum == nSteps-1){

				// Open our filestream
				char filename[20];
				sprintf(filename, "sim_%04i.fid", fidNum++);
				out.open(filename, ofstream::out);	

				// Final spin sum from all different gradients
				spinSum = spinSum + spin;
				spinSum = lowPassFilter(spinSum);
				addNoise(spinSum);

				// Write the results to a sim_%04d.fid file
				for (int i = 0; i < nPoints; i += reduction){ // Reduction to desired nPoints
					out << time[i] << " " << spinSum[i] << endl;
				}
				out.close();

				// Reset the counters
				index = 0; // They'll get incremented after.
				stepNum = 0;
				count = 0;

				// Progress report
				if (fidNum % (nfids/10) == 0){
					cout << fidNum << " FIDs have been simulated." << endl;
				}

			} else {

				// Add the spin to the set and do the next gradient
				spinSum = spinSum + spin;
				stepNum++;
				index = 0;
				count = 0;

			}
		}
	}
}

// Attempts to load parameters from a config file
void loadParams(string filename)
{

	// If default config is specified, return.
	if (boost::iequals("none", filename)) return;

	// Else load the property tree.
	ptree pt;
	read_json(filename, pt);

	// Now cycle through parameters, second argument is the default if not found.
	nfids = pt.get<int>("num_fids", nfids);
	nPoints = pt.get<int>("num_points", nPoints);
	reduction = pt.get<int>("reduction", reduction);

	ti = pt.get<double>("time_start", ti);
	tTotal = pt.get<double>("time_total", tTotal);
	tf = ti + tTotal;
	dt = pt.get<double>("time_delta", dt);

	fL = pt.get<double>("freq_larmor", fL);
	fRef = pt.get<double>("freq_reference", fRef);
	tPulse = pt.get<double>("time_pulse", tPulse);
	OmegaR = pt.get<double>("omega_r", OmegaR);
	gamma1 = pt.get<double>("gamma_1", gamma1);
	gamma2 = pt.get<double>("gamma_2", gamma2);
	gammag = pt.get<double>("gamma_g", gammag);

	nSteps = pt.get<int>("gradient_steps", nSteps);
	Brange = pt.get<double>("gradient_range", Brange);
}
